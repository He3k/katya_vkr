﻿using gpntb.Data;
using gpntb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;




       



namespace gpntb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, UserManager<IdentityUser> userManager, ApplicationDbContext context) 
        {
            _logger = logger;
            _userManager = userManager;
            _context = context;
        }


        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var phoneNumber = user != null ? await _userManager.GetPhoneNumberAsync(user) : "Number nothing";

            ViewData["PhoneNumber"] = phoneNumber;
            if (User.Identity.IsAuthenticated)
            {
                using (IWebDriver driver = new ChromeDriver())
                {

                    try
                    {
                        driver.Navigate().GoToUrl($"http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID={phoneNumber}");
                        IWebElement dataElement = driver.FindElement(By.CssSelector(".advanced tr td"));
                        string dataText = dataElement.GetAttribute("innerHTML").Trim();
                        string[] fullName = dataText.Split(new string[] { "<br>" }, StringSplitOptions.None);
                        if (fullName.Length >= 3)
                        {
                            string last_name = fullName[0].Trim();
                            string first_name = fullName[1].Trim();
                            string middle_name = fullName[2].Trim();

                            ViewData["last_name"] = last_name;
                            ViewData["first_name"] = first_name;
                            ViewData["middle_name"] = middle_name;
                        }
                    }
                    finally
                    {
                        driver.Quit();
                    }

                }
            }
            return View();
        }

        [Authorize]
        public class ProfileController : Controller
        {
            private readonly ApplicationDbContext _context;

            public ProfileController(ApplicationDbContext context)
            {
                _context = context;
            }

        }

        public IActionResult Privacy()
        {

            using (IWebDriver driver = new ChromeDriver())
            {
                
                try
                {
                    driver.Navigate().GoToUrl("http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID=0006491011036");
                    IWebElement dataElement = driver.FindElement(By.CssSelector(".advanced tr td"));
                    string dataText = dataElement.GetAttribute("innerHTML").Trim();
                    string[] fullName = dataText.Split(new string[] { "<br>" }, StringSplitOptions.None);
                    if (fullName.Length >= 3)
                    {
                        string last_name = fullName[0].Trim();
                        string first_name= fullName[1].Trim();
                        string middle_name = fullName[2].Trim();

                        ViewData["last_name"] = last_name;
                        ViewData["first_name"] = first_name;
                        ViewData["middle_name"] = middle_name;
                    }
                }
                finally
                {
                    driver.Quit();
                }

            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        static string GetValueByKey(string input, string startKey, string endKey)
        {
            int startPos = input.IndexOf(startKey) + startKey.Length;
            if (startPos < startKey.Length) return "";

            int endPos = string.IsNullOrEmpty(endKey) ? input.Length : input.IndexOf(endKey, startPos);
            if (endPos < 0) endPos = input.Length;

            string value = input.Substring(startPos, endPos - startPos).Trim();
            return value.Split(new char[] { ' ' }, 2)[0].Trim();
        }

        public async Task<IActionResult> Orders()
        {
            var user = await _userManager.GetUserAsync(User);
            var phoneNumber = user != null ? await _userManager.GetPhoneNumberAsync(user) : "Number nothing";

            ViewData["PhoneNumber"] = phoneNumber;
            if (User.Identity.IsAuthenticated)
            {
                using (IWebDriver driver = new ChromeDriver())
                {
                    try
                    {
                        driver.Navigate().GoToUrl($"http://webirbis.spsl.nsc.ru/irbis64r_01/cgi/cgiirbis_64.exe?C21COM=F&I21DBN=CAT_About&P21DBN=CAT&S21CNR=&Z21ID={phoneNumber}");
                        IWebElement myOrdersLinkElement = driver.FindElement(By.XPath("//a[contains(text(),'Мои заказы')]"));
                        string myOrdersLink = myOrdersLinkElement.GetAttribute("href");
                        driver.Navigate().GoToUrl($"{myOrdersLink}");
                        IReadOnlyList<IWebElement> advancedElements = driver.FindElements(By.ClassName("advanced"));
                        List<Dictionary<string, string>> orderDetailsList = new List<Dictionary<string, string>>();

                        foreach (IWebElement infoElement in advancedElements.Skip(1))
                        {
                            Dictionary<string, string> orderDetails = new Dictionary<string, string>();

                            string infoText = infoElement.Text;
                            Console.WriteLine($"Информация о заказе: {infoText}");

                            
                            orderDetails["databaseCatalog"] = GetValueByKey(infoText, "База данных каталога:", "Шифр хранения:");
                            orderDetails["storageCode"] = GetValueByKey(infoText, "Шифр хранения:", "История тела [Текст] :");
                            orderDetails["orderDate"] = GetValueByKey(infoText, "Дата заказа:", "Заказ забронирован");
                            orderDetails["storageLocation"] = GetValueByKey(infoText, "Место хранения:", "Место выдачи:");
                            orderDetails["issuePlace"] = GetValueByKey(infoText, "Место выдачи:", "");
                            bool isOrderBooked = infoText.Contains("Заказ забронирован");
                            string orderBooked = isOrderBooked ? "Да" : "Нет";
                            orderDetails["isOrderBooked"] = orderBooked;
                            orderDetailsList.Add(orderDetails);
                            
                            foreach (KeyValuePair<string, string> detail in orderDetails)
                            {
                                Console.WriteLine($"{detail.Key}: {detail.Value}");
                            }
                            
                        }
                        foreach (Dictionary<string, string> orderDetails in orderDetailsList)
                        {
                            var userId = _userManager.GetUserId(User);
                            var orderDate = orderDetails.ContainsKey("orderDate") && DateTime.TryParse(orderDetails["orderDate"], out DateTime tempOrderDate) ? tempOrderDate : DateTime.Now;
                            var orderDatabase = orderDetails.ContainsKey("databaseCatalog") ? orderDetails["databaseCatalog"] : "1";
                            // Предположим, что комбинация UserId, OrderDatabase и OrderDate делает заказ уникальным.
                            
                            // Проверяем, есть ли уже такой заказ в базе данных.
                            var existingOrder = _context.Orders
                                .FirstOrDefault(o => o.UserId == userId && 
                                                    o.OrderDatabase == orderDatabase && 
                                                    o.OrderDate.Date == orderDate.Date); // Сравниваем только дату без времени, если это необходимо.
                            
                            // Если такой заказ не найден, создаем новый.
                            if (existingOrder == null)
                            {
                                var order = new Order
                                {
                                    OrderDatabase = orderDatabase,
                                    OrderChipher = orderDetails.ContainsKey("storageCode") ? orderDetails["storageCode"] : "2",
                                    OrderPlaceholder = orderDetails.ContainsKey("storageLocation") ? orderDetails["storageLocation"] : "3",
                                    OrderPlaceIssue = orderDetails.ContainsKey("issuePlace") ? orderDetails["issuePlace"] : "4",
                                    OrderBron = orderDetails.ContainsKey("isOrderBooked") ? orderDetails["isOrderBooked"] : "5",
                                    OrderDate = orderDate,
                                    UserId = userId,
                                    OrderComment = orderDetails.ContainsKey("orderComment") ? orderDetails["orderComment"] : ""
                                };

                                _context.Orders.Add(order);
                            }
                        }
                        await _context.SaveChangesAsync();
                            foreach (Dictionary<string, string> orderDetails in orderDetailsList)
                            {
                                Console.WriteLine("--------------------------------");
                                Console.WriteLine("Details:");
                                foreach (KeyValuePair<string, string> detail in orderDetails)
                                {
                                    Console.WriteLine($"{detail.Key}: {detail.Value}");
                                }
                                Console.WriteLine("--------------------------------");
                            }
                        
                        if (orderDetailsList.Any())
                        {
                            ViewData["OrderDetailsList"] = orderDetailsList;
                            
                        }
                        else
                        {
                            Console.WriteLine("Need elem 'advanced' on page.");
                        }
                    }
                    finally
                    {
                        driver.Quit();
                    }

                }
            }
            return View();
        }
        public IActionResult NewOrder()
        {


            return View();
        }
        public IActionResult LongerOrder()
        {


            return View();
        }
        // public IActionResult List()
        // {
            

        //     return View();
        // }
        
        public async Task<ActionResult> List()
        {
            var orders = await _context.Orders
                .Select(o => new OrderViewModel
                {
                    OrderDatabase = o.OrderDatabase,
                    OrderChipher = o.OrderChipher,
                    OrderPlaceholder = o.OrderPlaceholder,
                    OrderPlaceIssue = o.OrderPlaceIssue,
                    OrderBron = o.OrderBron,
                    OrderDate = o.OrderDate,
                    OrderComment = o.OrderComment // Убедитесь, что ваша БД содержит это поле
                }).ToListAsync();

            return View(orders);
        }

    }
}
