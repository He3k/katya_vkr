using Microsoft.AspNetCore.Identity;

namespace gpntb.Models // Измените на актуальное пространство имён вашего проекта
{
    public class ApplicationUser : IdentityUser
    {
        // Вы можете добавить дополнительные поля, которые хотите ассоциировать с пользователем
    }
}
