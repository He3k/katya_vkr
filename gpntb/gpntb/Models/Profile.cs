﻿using Microsoft.AspNetCore.Identity;

namespace gpntb.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public IdentityUser User { get; set; }
    }

}
