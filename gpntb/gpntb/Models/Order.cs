using gpntb.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
public class Order
{
    [Key]
    public int OrderId { get; set; }
    
    [Required]
    public string OrderDatabase { get; set; } // Название книги или ресурса
    public string OrderChipher { get; set; } // Название книги или ресурса
    public string OrderPlaceholder { get; set; } // Название книги или ресурса
    public string OrderPlaceIssue { get; set; } // Название книги или ресурса
    public string OrderBron { get; set; } // Название книги или ресурса
    public DateTime OrderDate { get; set; } // Дата заказа

    // Другие атрибуты, связанные с заказом
    public string OrderComment { get; set; } // Комментарии для перечня
    // Внешний ключ для пользователя (Identity)
    public string UserId { get; set; }

    // Навигационное свойство
    [ForeignKey("UserId")]
    public virtual ApplicationUser User { get; set; }
}
