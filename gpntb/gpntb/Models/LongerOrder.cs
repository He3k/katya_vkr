using gpntb.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
public class LongerOrder
{
    [Key]
    public int LongerOrderId { get; set; }
    
    [Required]
    public string OrderId { get; set; } 
    public DateTime OrderDate { get; set; } // Дата заказа
    public DateTime LongerOrderDate { get; set; }
    public DateTime CurrenOrderDate { get; set; } // Текущая дата завершения заказа
    public DateTime NowDate { get; set; }
    public string OrderStatus { get; set; }

    // Другие атрибуты, связанные с заказом
    // Внешний ключ для пользователя (Identity)
    public string UserId { get; set; }

    // Навигационное свойство
    [ForeignKey("UserId")]
    public virtual ApplicationUser User { get; set; }
}