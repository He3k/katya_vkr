using gpntb.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

    public class OrderViewModel
    {
        public string OrderDatabase { get; set; }
        public string OrderChipher { get; set; }
        public string OrderPlaceholder { get; set; }
        public string OrderPlaceIssue { get; set; }
        public string OrderBron { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderComment { get; set; } // Добавленное поле для комментария
    }
