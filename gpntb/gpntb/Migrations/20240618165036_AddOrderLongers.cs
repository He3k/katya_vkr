﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace gpntb.Migrations
{
    /// <inheritdoc />
    public partial class AddOrderLongers : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LongerOrders",
                columns: table => new
                {
                    LongerOrderId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LongerOrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CurrenOrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NowDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OrderStatus = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LongerOrders", x => x.LongerOrderId);
                    table.ForeignKey(
                        name: "FK_LongerOrders_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LongerOrders_UserId",
                table: "LongerOrders",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LongerOrders");
        }
    }
}
