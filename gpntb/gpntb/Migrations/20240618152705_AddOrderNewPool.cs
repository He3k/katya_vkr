﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace gpntb.Migrations
{
    /// <inheritdoc />
    public partial class AddOrderNewPool : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Orders",
                newName: "OrderPlaceholder");

            migrationBuilder.AddColumn<string>(
                name: "OrderBron",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "OrderChipher",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "OrderDatabase",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "OrderPlaceIssue",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderBron",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderChipher",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderDatabase",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderPlaceIssue",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "OrderPlaceholder",
                table: "Orders",
                newName: "Title");
        }
    }
}
