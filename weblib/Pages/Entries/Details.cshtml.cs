using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using weblib.Data;
using weblib.Models;

namespace weblib.Pages_Entries
{
    public class DetailsModel : PageModel
    {
        private readonly weblib.Data.EntryContext _context;

        public DetailsModel(weblib.Data.EntryContext context)
        {
            _context = context;
        }

        public Entry Entry { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entry = await _context.Entry.FirstOrDefaultAsync(m => m.Id == id);
            if (entry == null)
            {
                return NotFound();
            }
            else
            {
                Entry = entry;
            }
            return Page();
        }
    }
}
