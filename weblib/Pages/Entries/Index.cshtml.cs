using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using weblib.Data;
using weblib.Models;

namespace weblib.Pages_Entries
{
    public class IndexModel : PageModel
    {
        private readonly weblib.Data.EntryContext _context;

        public IndexModel(weblib.Data.EntryContext context)
        {
            _context = context;
        }

        public IList<Entry> Entry { get;set; } = default!;

        public async Task OnGetAsync()
        {
            Entry = await _context.Entry.ToListAsync();
        }
    }
}
